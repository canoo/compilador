#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/** > 3E . 2E 
 < 3C , 2C
 + 2B [ 5B
 - 2D ] 5D **/


//unsigned char *pila;

typedef struct Nodo_struct
{
	char c;
	struct Nodo_struct **nodoshijos;
	struct Nodo_struct *nodopadre;
	int nhijos;
	int raiz;
}Nodo;


char * procesaSimbolo(char c);
void liberaMemoria(Nodo* nodo);
void recorreProfundidad(Nodo* nodo);
Nodo * init_nodo(char c);
void obtenString(Nodo * n, char * buffer);


int main(int argc, char* argv[])
{
	FILE *fp;
		
	if(argc == 1) return -1; 

	printf("fichero: %s \n",argv[1]);
	fp = fopen ( argv[1], "r" );        
	if (fp==NULL) {fputs ("File error",stderr); exit (1);}

	char caracter;
	Nodo *padre = init_nodo(fgetc(fp)); 
	*(padre->nodoshijos) =(Nodo *) malloc(sizeof(Nodo)*1000);
	padre->raiz = 1;
	padre->nodopadre = padre;
	Nodo *padredirecto= padre;
	int longitud = 0;

	while((caracter = fgetc(fp)) != EOF){
		
		
		if((caracter != 0x3C) && (caracter!= 0x2C) && (caracter != 0x2B) && (caracter != 0x5B)&& (caracter !=0x2D) && (caracter != 0x5D)&&(caracter != 0x3E)&&(caracter != 0x2E)){ 
			continue;
		}

		Nodo *n = init_nodo(caracter);
		n->nodopadre = padredirecto;
		padredirecto->nodoshijos[padredirecto->nhijos] = n;
		padredirecto->nhijos++;
		
		if(caracter == 0x5B){ // [
			*(n->nodoshijos) = (Nodo *)malloc(sizeof(Nodo)*1000);
			padredirecto = n;

		} else if (caracter == 0x5D){ // ]
			
			if(padredirecto == padre){
				printf("Has cerrado todos los corchetes\n");
				return -1;
			}
	
			padredirecto = padredirecto->nodopadre;
		}
		longitud ++;
	}
	
	if(padredirecto != padre){
		printf("Error analisis sintactico, no has cerrado todos los corchetes\n");
		return -1;
	}
		

	fclose ( fp );

//	recorreProfundidad(padre);
	char *nombre = "ficheroBF.c";

	fp = fopen ( nombre, "w" ); 

	fprintf(fp,"#include<stdlib.h>\n#include<stdio.h>\nunsigned char ptr[30000];\nint i=0;\nint main(){\n");
	
	char * buffer = calloc(longitud,20); //mas o menos cada linea tiene unos 20 caracteres (como maximo)

	obtenString(padre,buffer);
	
	fprintf(fp,"%s \n",buffer);
	fprintf(fp,"}\n");
	fclose ( fp );
	
	free(buffer);
	liberaMemoria(padre);

	return 0;
}

void liberaMemoria(Nodo *n){
	for(int i=0; i<n->nhijos;i++){
		liberaMemoria(n->nodoshijos[i]);
	}

	if(n->nhijos>0){
		free(*n->nodoshijos);
	}
}

char * procesaSimbolo(char c){


	switch(c){
		case 0x3E: //>
			return "i++;";	
		break;

		case 0x2E: // .
			return "putchar(ptr[i]);";
		break;
	
		case 0x3C: //<
			return "i--;";
		break;
	
		case 0x2C: // ,
			return "ptr[i]=getchar();";
		break;

		case 0x2B: //+
			return "ptr[i]++;";
		break;

		case 0x5B: //[
			return "while(ptr[i]){";
		break;

		case 0x2D: //-
			return "ptr[i]--;";
		break;

		case 0x5D: //]
			return"}";
		break;

		default:
		return "";
}


}

void obtenString(Nodo* nodo,char * buffer){
	
	strcat(buffer,procesaSimbolo(nodo->c));
	for(int i =0; i< nodo->nhijos; i++){
		obtenString(nodo->nodoshijos[i],buffer);
	}

}
	
void recorreProfundidad(Nodo* nodo){
	printf("%c",nodo->c);
	for(int i =0; i< nodo->nhijos; i++){
		recorreProfundidad(nodo->nodoshijos[i]);
	}

}

Nodo * init_nodo(char c){
	Nodo *n = malloc(sizeof(Nodo));
	n->nhijos=0;
	n->nodoshijos = malloc(sizeof(Nodo **));
	n->nodopadre = malloc(sizeof(Nodo *));
	n->c=c; 
	return n;
}
