#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/** > 3E . 2E 
 < 3C , 2C
 + 2B [ 5B
 - 2D ] 5D **/


//unsigned char *pila;

typedef struct Nodo_struct
{
	char c;
	struct Nodo_struct **nodoshijos;
	struct Nodo_struct *nodopadre;
	int nhijos;
	int terminal;
}Nodo;


void programa(char c);
void liberaMemoria(Nodo* nodo);
void recorreProfundidad(Nodo* nodo);
Nodo * init_nodo(char c,int t, Nodo * padre);


Nodo *actual;

int main(int argc, char* argv[])
{
	FILE *fp;
		
	if(argc == 1) return -1; 

	printf("fichero: %s \n",argv[1]);
	fp = fopen ( argv[1], "r" );        
	if (fp==NULL) {fputs ("File error",stderr); exit (1);}

	char caracter;
	Nodo *padre = init_nodo('S',0,NULL); 
	padre->nodopadre = padre;
	actual= padre;
	int longitud = 0;

	while((caracter = fgetc(fp)) != EOF){
		
		
		if((caracter != 0x3C) && (caracter!= 0x2C) && (caracter != 0x2B) && (caracter != 0x5B)&& (caracter !=0x2D) && (caracter != 0x5D)&&(caracter != 0x3E)&&(caracter != 0x2E)){ 
			continue;
		}

		if(actual->terminal){
			printf("Error analisis sintactico");
			return 0;
		}	

		programa(caracter);

		longitud ++;
	}
	
	if(!actual->terminal){
		programa(' ');
	}

	while(actual != actual->nodopadre){
	
		actual = actual->nodopadre;
		for(int i=0; i<actual->nhijos;i++){
			if(!actual->nodoshijos[i]->terminal){
				programa(' ');
				actual = actual->nodopadre;
				break;
			}
		}		

	}
	

	fclose ( fp );
//	recorreProfundidad(padre);
	


	puts(" ");
	
	return 0;
}

void liberaMemoria(Nodo *n){
	for(int i=0; i<n->nhijos;i++){
		liberaMemoria(n->nodoshijos[i]);
	}

	if(n->nhijos>0){
		free(*n->nodoshijos);
	}
}

void programa(char c){


	switch(c){
		case 0x3E: //>

		case 0x2E: // .
	
		case 0x3C: //<
	
		case 0x2C: // ,

		case 0x2B: //+

		case 0x2D: //-
			*(actual->nodoshijos) = malloc(sizeof(Nodo*) * 2);
			actual->nodoshijos[actual->nhijos]=  init_nodo(c,1,actual);
			actual->nodoshijos[actual->nhijos+1]= init_nodo('S',0,actual);
			actual->nhijos+=2;
			actual->terminal=1;
			actual = actual->nodoshijos[actual->nhijos-1];
			break;

		case 0x5B: //[
						
			*(actual->nodoshijos) = malloc(sizeof(Nodo*) * 4);
			actual->terminal =1;
			actual->nodoshijos[actual->nhijos]=init_nodo(c,1,actual);
			actual->nodoshijos[actual->nhijos+1]=init_nodo('S',0,actual);
			actual->nodoshijos[actual->nhijos+2]=init_nodo(']',1,actual);
			actual->nodoshijos[actual->nhijos+3]=init_nodo('S',0,actual);
			actual->nhijos+=4;
			actual = actual->nodoshijos[actual->nhijos-3];
			break;

		case 0x5D: //]
			actual->terminal=1;
			actual->c= ' ';

			while(actual->nodopadre != actual){
				actual = actual->nodopadre;
				for(int i=0; i<actual->nhijos;i++){
					if(!actual->nodoshijos[i]->terminal){
						actual = actual->nodoshijos[i];
						return;
					}
				}		
			}
			actual = init_nodo('F',1,actual);
			break;
		case ' ':
			actual->terminal=1;
			actual->c= ' ';
			break;


}


}

	
void recorreProfundidad(Nodo* nodo){
	printf("%c",nodo->c);
	if(c->nhijos == 0){

		//Ejecuta la instruccionn
	}

	for(int i =0; i< nodo->nhijos; i++){
		recorreProfundidad(nodo->nodoshijos[i]);
	}

}

Nodo * init_nodo(char c,int t, Nodo * padre){
	Nodo *n = malloc(sizeof(Nodo));
	n->nhijos=0;
	n->nodoshijos = malloc(sizeof(Nodo **));
	n->nodopadre = malloc(sizeof(Nodo *));
	n->nodopadre = padre;
	n->c=c;
       	n->terminal = t;

	return n;
}
