#include <stdio.h>
#include <stdlib.h>
#include <functional>

std::function<void()> programa();
void siguiente();

FILE *fp;

unsigned char* ptr; //pila para la ejecucion del programa brainfuck
char actual;

int main(int argc, char* argv[])
{
	ptr= (unsigned char *) malloc(30000);
		
	if(argc == 1) return -1; 
	printf("fichero: %s \n",argv[1]);
	fp = fopen ( argv[1], "r" );        
	if (fp==NULL) {fputs ("File error",stderr); exit (1);}
	siguiente();

	auto pp = programa();
	pp();
	fclose ( fp );
	return 0;
}

void siguiente(){
	actual = fgetc(fp);
}

std::function<void()> programa(){

	char c = actual;
	switch(c){

		case '>': //>
		{
			siguiente();
			auto funcion = programa();
		 	auto ejecuta=[&,funcion](){	
				++ptr;
				funcion();
			};
			return ejecuta;
		}
			
		case ',': // .
		{
			siguiente();	
			auto funcion2 = programa();
			auto ejecuta2= [&,funcion2](){
				*ptr = getchar();
				funcion2();
			};
			return ejecuta2;
		}
	
		case '<': //<
		{	siguiente();
			auto funcion3 = programa();
			auto ejecuta3=[&,funcion3](){
				--ptr;
				funcion3();
			};
			return ejecuta3;
		}
		case '.': // ,
		{	siguiente();
			auto funcion4 = programa();
			auto ejecuta4=[&,funcion4](){
				putchar(*ptr);
				funcion4();
			};

			return ejecuta4;
		}

		case '+': //+
		{	siguiente();
				
			auto funcion5 = programa();
			auto ejecuta5= [&,funcion5](){
				++(*ptr);
				funcion5();
			};
			return ejecuta5;
		}

		case '-': //-
		{	siguiente();
			auto funcion6 = programa();
			auto ejecuta6= [&,funcion6](){
				(*ptr)--;
				funcion6();
			};
			return ejecuta6;
		}

		case '[': //[		
		{	siguiente();		
			auto funcion7 = programa();
				
			if(actual != 0x5D){
				//Falloooooo
			}

			siguiente();
			auto funcion8 = programa();
			auto ejecuta7= [&,funcion8,funcion7](){
				while(*ptr){
					funcion7();
				}

				funcion8();
			};

			return ejecuta7; 
		}	
		case 0x5D: //]
		case EOF:
		
			return [](){};
		default:
			siguiente();
			return programa();
	}
}	
