tokens = [12, '*', 12, '$']

actual = tokens.pop(0)

def siguiente():
    global actual
    actual = tokens.pop(0)

def acepta(t):
    if actual == t:
        siguiente()
        return True
    return False


def espera(t):
    if not acepta(t):
        raise Exception(f"Se esperaba {t}")


def factor():
    if acepta('('):
        g = expresion()
        if espera(')'):
            return g
    else:
        j = actual
        def numero():
            return j
        siguiente()
        return numero
# multiplicacion : factor * multiplicacion | factor
def multiplicacion():
    f = factor()
    if acepta('*'):
        h = multiplicacion()
        def temp():
            return f()*h()
        return  temp
    return f
            
def expresion():
    f = multiplicacion()
    if acepta('+'):
        h = expresion()
        def temp():
            return f() + h()
        return temp
    return f


#print("El resultado es ", expresion()())
print(multiplicacion()())
